# interseismic_practical
Practical exercise on interseismic strain accumulation for the COMET InSAR course.  
please run the [practicals here](https://mybinder.org/v2/gl/comet_licsar%2Finterseismic_practical/HEAD?labpath=Interseis_practical.ipynb)
